#!/bin/bash

[ -z "$2" ] && echo "usage: $0 SOURCE_DIR DEST_DIR" && exit 0
[ ! -d "$1" ] && echo "$1 is not a directory. Aborting." && exit 1
[ -a "$2" ] && echo "$2 already exists. Aborting." && exit 1

src=${1%/}
dest=$2
l=0

mkdir "$dest"
cd "$src"
for sgf in $(find -mindepth 1 | cut -d/ -f2-); do
	sgfs="$sgfs $sgf"
	d=${#sgf}
	(( d > l )) && let l=$d
done
cd "../$dest"
for sgf in $sgfs; do
	link="$sgf"
	while (( ${#link} < l )); do
		link="${link:9}"
		link="OGS_game_0$link"
	done
	ln -s "../${src}/${sgf}" "$link"
done

